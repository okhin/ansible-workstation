# We want some powerlines config
if [ -e /usr/share/powerline/bindings/bash/powerline.sh ]
then
    POWERLINE_BASH_CONTINUATION=1
    POWERLINE_BASH_SELECT=1
    source /usr/share/powerline/bindings/bash/powerline.sh
fi

#!/bin/bash
# Adds .local/bin and .local/AppImages as path
export PATH=$HOME/.local/bin:$HOME/.local/AppImages:$PATH

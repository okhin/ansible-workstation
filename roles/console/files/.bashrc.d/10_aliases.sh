# This file sets some usefull alias

alias ls="ls --color=auto"
[[ -x $(which exa) ]] && alias als="exa -lTF -snew -L 2 --git"

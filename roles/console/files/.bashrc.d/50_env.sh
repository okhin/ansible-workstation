# Let's export some env variables
export EDITOR="/usr/bin/editor"
export VISUAL="/usr/bin/view"
export PAGER="/usr/bin/less"
export MANPAGER="/usr/bin/less +Gg"

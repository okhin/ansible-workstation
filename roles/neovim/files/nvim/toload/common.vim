" Matchit to enhance % jump
Plug 'adelarsq/vim-matchit'

" Auto pair help to close stuff opened, with nice indent
Plug 'tpope/vim-surround'

" Nifty baseline
Plug 'vim-airline/vim-airline'

" The TagBar is usefull to navigate in one file
Plug 'majutsushi/tagbar'

" Ag plugin
Plug 'rking/ag.vim', { 'on': 'Ag' }

" And async grep
Plug 'mhinz/vim-grepper', { 'on': ['Grepper', '<Plug>(GrepperOperator)'] }

" Magit to work anywhere with git
Plug 'jreybert/vimagit'

" Bind tmux and vim switching pane together
Plug 'christoomey/vim-tmux-navigator'

" nicer color theme
Plug 'reedes/vim-colors-pencil'

" neomake
Plug 'neomake/neomake'

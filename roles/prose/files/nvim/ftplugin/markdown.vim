" Let's define where grammalecte is
let g:grammalecte_cli_py='/usr/local/bin/grammalecte-cli.py'

" Some features used for code are of low interest here
" No autocomplete for prose text, it's annoying
let g:asyncomplete_auto_popup = 0

set spelllang=fr
set spell
set nonumber

" Let's add an autocommand on write
augroup prose
    autocmd!
    autocmd FileType markdown,mkd call pencil#init({'wrap': 'hard'})
    autocmd BufWritePost markdown,mkd :GrammalecteCheck
augroup END

" Toggling Goyo
nmap <F10> :Goyo<CR>

" Tell airline what to do with Pencil
let g:airline_section_x = '%{PencilMode()}'

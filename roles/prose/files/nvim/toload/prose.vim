" Load the grammalecte  plugin
Plug 'dpelle/vim-Grammalecte', { 'for': 'markdown'}

" Load the vim-pencil
Plug 'reedes/vim-pencil', { 'for': 'markdown'}

" Load distraction free vim
Plug 'junegunn/goyo.vim', { 'for': 'markdown'}
